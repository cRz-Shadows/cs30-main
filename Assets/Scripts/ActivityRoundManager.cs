using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivityRoundManager : MonoBehaviour
{

    public RoundTimer Timer;
    public GameObject[] OrderedRoundDecks;
    private int currentDeck;

    void UpdateActiveDeck()
    {
        if (currentDeck >= OrderedRoundDecks.Length)
        {
            SceneLoader.Load(EnumGameRound.CONTEXT_EVENT);
        }
        else
        {
            for (int i = 0; i < OrderedRoundDecks.Length; i++)
            {
                SpawnCard cardSpawnScript = OrderedRoundDecks[i].GetComponent<SpawnCard>();
                cardSpawnScript.Active = i == currentDeck;
            }
        }
    }

    void Start()
    {
        if (OrderedRoundDecks.Length > 0)
        {
            //currentDeck = 0;
            //UpdateActiveDeck();
            //Timer.StartTimer();
        }
    }

    void Update()
    {
        if (Timer.finished && currentDeck < OrderedRoundDecks.Length)
        {
            //Timer.ResetTimer();
            //currentDeck += 1;
            //UpdateActiveDeck();
        }
    }
}
