using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[RequireComponent(typeof(BoxCollider2D))]
public class SpawnEventCard : MonoBehaviour
{
    /*
        cards_for_deletion - A list of all cards that the player can delete by clicking
        held_event_cards - A list of all held event card of the player
        held_event_card_index - Keeps track of the index of each event card in the player's deck. This tells the system what function the card has
        cards_for_turnover - A list of all card that that player can turn over by clicking
        event_card_layer - increases every time an event card is played, makes sure each new card is placed at the top of the deck
        spawnSpecific - if true, will spawn a specific event card based on the variable spawnNumber passed into spawnEventCard()
    */
    public GameObject EventCard;
    public Sprite[] EventSprites;

    public static bool canDraw = true;
    public static bool spawnSpecific = false;

    public static List<GameObject> cards_for_deletion = new List<GameObject>();
    public static List<GameObject> held_event_cards = new List<GameObject>();
    public static List<GameObject> cards_for_turnover = new List<GameObject>();
    public static List<int> held_event_card_index = new List<int>();
    public static int no_cards_to_delete = 0;
    public static int no_cards_to_turnover = 0;
    public static int event_card_layer = 1;
    public static string event_card_colour;
    public List<int> event_card_deck = new List<int>();

    void OnMouseDown()
    {
        spawnEventCard(this.gameObject);
    }

    public void removeEventCard(string name)
    {
        // If a deck runs out, repopulate it
        if (!event_card_deck.Any()){
            event_card_deck = Enumerable.Range(0, EventSprites.Count()).ToList();
        }
        foreach(int index in event_card_deck)
        {
            if(EventSprites[index].name==name)
            {
                event_card_deck.Remove(index);
                break;
            }
        }
       
    }

    public void spawnEventCard(GameObject currentObject, bool spawnSpecific=false, int spawnNumber = -1)
    {
        if (canDraw == true)
        {
            // create new card object
            transform.position = new Vector3(transform.position.x, transform.position.y, -1);
            GameObject newEventCard = GameObject.Instantiate(EventCard, transform.position, transform.rotation);
            SpriteRenderer cardSpriteRenderer = newEventCard.GetComponent<SpriteRenderer>();
            event_card_layer += 1;

            if (spawnSpecific == false){
                Destroy(newEventCard.GetComponent<BoxCollider2D>(),2);
            }

            // get colour of current card
            if (currentObject == GameObject.Find("Event_Card_Draw_Red"))
            {
                event_card_colour="red";
            }
            if (currentObject == GameObject.Find("Event_Card_Draw_Green"))
            {
                event_card_colour="green";
            }
            if (currentObject == GameObject.Find("Event_Card_Draw_Blue"))
            {
                event_card_colour="blue";
            }

            // If a deck runs out, repopulate it
            if (!event_card_deck.Any()){
                event_card_deck = Enumerable.Range(0, EventSprites.Count()).ToList();
            }

            // select random card from current colour deck
            int rand_num = 0;
            if(spawnSpecific == false)
            {
                int temp = Random.Range(0,  event_card_deck.Count());
                rand_num = event_card_deck[temp];
                event_card_deck.RemoveAt(temp);
            }
            else
            {
                rand_num = spawnNumber;
                held_event_cards.Add(newEventCard);
            }

            cardSpriteRenderer.sprite = EventSprites[rand_num];
            cardSpriteRenderer.sortingOrder = event_card_layer;
            GameManager.INSTANCE.NetworkManager.SendPacket(new CDrawEventPacket(cardSpriteRenderer.sprite.name, transform.position.x, transform.position.y));

            if (spawnSpecific == false)
            {
                newEventCard.transform.gameObject.layer = LayerMask.NameToLayer("Ignore Raycast");
                functions(rand_num, event_card_colour);
            }
            else
            {
                newEventCard.AddComponent<SpriteDrag>();
                newEventCard.AddComponent<EventCardFunctions>();
            }
        }
    }

    /*
        This function adds the actions of every event card in the deck.
        It finds the card that was drawn and executes the actions on the card
    */
    async void functions(int rand_num, string event_card_colour)
    {
        GameObject object_to_spawn;
        List<int> position_of_cards_on_board = new List<int>();
        List<int> card_index_list = SpawnCard.GetList_index();
        List<string> card_colour_list = SpawnCard.GetList_colour();
        int counter = 0;
        bool exceptions = false;
        
        if (event_card_colour=="red")
        {
            switch (rand_num)
            {
                case 0:
                    // Advice from academic
                    // You may search through the PLAN deck and (if possible) add an ethical clearence tile to your plan section
                    object_to_spawn = GameObject.Find("BACK-act-plan-01");
                    object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 0);
                    break;
                case 1:
                    // Advice from specialist
                    // You may remove 1 useless article tile from the context section
                    foreach(int i in card_index_list)
                    {
                        if ((i==4) && (card_colour_list[counter]=="red"))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if (cards_for_deletion.Count>0)
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    break;
                case 2:
                    // Bluescreen!
                    // Remove any 3 tiles from the current section
                    // if plan contains research data management strategy, ignore effect
                    bluescreen(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 3: case 4:
                    // Distraction
                    // Remove any 2 tiles from current section
                    // if plan contains milestones only remove 1 tile
                    distraction(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 5:
                    // Tried to do too much?
                    // remove any 2 tiles from the current section
                    // if context contains refine research questions, ignore effect
                    tried_to_do_too_much(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 6:
                    // Held Card

                    // Expertise!
                    // If context contains at least 2 methodology tile, save this card
                    // you may use it at any point for the rest of the game to stand in for an expertise training plan tile
                    int methodology_counter = 0;
                    foreach(int i in card_index_list)
                    {
                        if ((i==4) && (card_colour_list[counter]=="red"))
                        {
                            methodology_counter += 1;
                        }
                        counter += 1;
                    }
                    if (methodology_counter>=2)
                    {
                        object_to_spawn = GameObject.Find("Player_Held_Event_Card_Spawner");
                        object_to_spawn.GetComponent<SpawnEventCard>().spawnEventCard(object_to_spawn, true, 6);
                        held_event_card_index.Add(1);
                    }
                    break;
                case 7:
                    // Great resource!
                    // search through the implementation deck and take a great resources tile
                    // next round you may play this tile in your implementation section
                    object_to_spawn = GameObject.Find("BACK-act-imp-01");
                    object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 8);
                    break;
                case 8:
                    // Soneone has my library book!
                    // remove 1 relevant or very relevant article tile from the context section
                    // if plan contains cintingency time, turn over that tile and ignore this effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==3)||(i==20)||(i==21))&&(card_colour_list[counter]=="red"))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 9: case 10:
                    // Lit review paradox
                    // block out 2 spaces in both the implementation and write-up sections
                    // if context contains refine research questions or 2 or more very relevant article tiles, ignore effect.
                    foreach(int i in card_index_list)
                    {
                        int temp1 = 0;
                        if ((i==3)&&(card_colour_list[counter]=="red"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                        if ((i==24)&&(card_colour_list[counter]=="red"))
                        {
                            temp1 += 1;
                            if (temp1 >= 2)
                            {
                                exceptions = true;
                            }
                        }
                    }
                    if (exceptions==false)
                    {
                        object_to_spawn = GameObject.Find("BACK-act-imp-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn = GameObject.Find("BACK-act-write-up-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                    }
                    break;
                case 11:
                    // interlibrary load delay
                    // remove 1 relevant or very relevant article tile from the current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==3) || (i==20) || (i==21)) && card_colour_list[counter]=="red")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                            cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    if ((cards_for_turnover.Count>=1)&&(exceptions==true))
                    {
                        no_cards_to_turnover = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_turnover.Clear();
                        no_cards_to_turnover = 0;
                    }
                    break;
                case 12:
                    // lost enthusiasm
                    // remove any 1 tile from current section
                    // if plan contains holiday time, ignore effect
                    lost_enthusiasm(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 13:
                    // mentor leaves
                    // remove all meetings with supervisor tiles from the plan section
                    int temp2 = 0;
                    foreach(int i in card_index_list)
                    {
                        if ((i==7) && card_colour_list[counter]=="yellow")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            temp2 += 1;
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=0)&&(exceptions==false))
                    {
                        no_cards_to_delete = temp2;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 14: case 15:
                    // held card

                    // multiple perspectives
                    // if context contains more than one type of resource tile (e.g. lab/archive), save this card
                    // you may use it at any point to ignore the effects of a tunnel vision card
                    int resource_counter = 0;
                    foreach(int i in card_index_list)
                    {
                        if (((i==6)||(i==7)||(i==8)||(i==10)||(i==11)||(i==13)||(i==14)||(i==15)||(i==16)||(i==17)||(i==18)||(i==19)||(i==22)) && (card_colour_list[counter]=="red"))
                        {
                            resource_counter += 1;
                        }
                        counter += 1;
                    }
                    if (resource_counter>=2)
                    {
                        object_to_spawn = GameObject.Find("Player_Held_Event_Card_Spawner");
                        object_to_spawn.GetComponent<SpawnEventCard>().spawnEventCard(object_to_spawn, true, 14);
                        held_event_card_index.Add(2);
                    }
                    break;
                case 16:
                    // procrastinare!
                    // remove any 3 tiles from the current section
                    // if plan contains milestones ignore effect, but turn over a milestones tile as your deadlines go sailing by
                    procrastinate(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 17:
                    // shoddy notes
                    // block out 2 spaces in both the implementation and write up sections
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    foreach(int i in card_index_list)
                    {
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                            cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if (exceptions==false)
                    {
                        object_to_spawn = GameObject.Find("BACK-act-imp-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn = GameObject.Find("BACK-act-write-up-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                    }
                    if ((cards_for_turnover.Count>=1)&&(exceptions==true))
                    {
                        no_cards_to_turnover = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_turnover.Clear();
                        no_cards_to_turnover = 0;
                    }
                    break;
                case 18:
                    // sick!
                    // remove any 2 tiles from the current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    sick(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 19:
                    // sister's wedding
                    // remove any 1 tiles from current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    foreach(int i in card_index_list)
                    {
                        if (card_colour_list[counter]=="red")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                            cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    if ((cards_for_turnover.Count>=1)&&(exceptions==true))
                    {
                        no_cards_to_turnover = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_turnover.Clear();
                        no_cards_to_turnover = 0;
                    }
                    break;
                case 20:
                    // Evaluation of sources fail!
                    // remove 2 tiles from the current section
                    foreach(int i in card_index_list)
                        {
                            if (card_colour_list[counter]=="red")
                            {
                                position_of_cards_on_board.Add(counter);
                                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            }
                            counter += 1;
                        }
                        if ((cards_for_deletion.Count>=2)&&(exceptions==false))
                        {
                            no_cards_to_delete = 2;
                            canDraw = false;
                        }
                        else {
                            cards_for_deletion.Clear();
                            no_cards_to_delete = 0;
                        }
                        break;
                case 21:
                    // Evaluation of sources win!
                    // you may search through the context deck and (if possible) add a very relevant article tile to the current section
                    object_to_spawn = GameObject.Find("BACK-act-context-01");
                    object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 3);
                    break;
                case 22:
                    // tunnel vision
                    // remove any 1 tile from current section
                    // if plan contains meetings with supervisor or context contains discussion with experts or refine research questions, ignore effect
                    tunnel_vision(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "red");
                    break;
                case 23:
                    // Writer's block
                    // block out 2 spaces in both the implementation and write-up sections
                    // if plan contains milestones and meeting with supervisor, ignore this effect
                    bool contains_milestones = false;
                    bool contains_meeting_with_supervisor = false;
                    foreach(int i in card_index_list)
                    {
                        if ((i==3)&&(card_colour_list[counter]=="yellow"))
                        {
                            contains_milestones = true;
                        }
                        if ((i==7)&&(card_colour_list[counter]=="yellow"))
                        {
                            contains_meeting_with_supervisor = true;
                        }
                        counter += 1;
                    }
                    if (contains_milestones && contains_meeting_with_supervisor)
                    {
                        exceptions = true;
                    }
                    if (exceptions==false)
                    {
                        object_to_spawn = GameObject.Find("BACK-act-imp-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 14);
                        object_to_spawn = GameObject.Find("BACK-act-write-up-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                    }
                    break;
            }
        }

        if (event_card_colour=="green")
        {
            switch (rand_num)
            {
                case 0:
                    // Bluescreen!
                    // Remove any 3 tiles from the current section
                    // if plan contains research data management strategy, ignore effect
                    bluescreen(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 1: case 2:
                    // Broken fridge!
                    // remove 3 data, results or great results tiles from the current section
                    foreach(int i in card_index_list)
                    {
                        if (((i==4) || (i==5) || (i==6) || (i==8) || (i==9) || (i==12) || (i==13)) && (card_colour_list[counter]=="green"))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if (cards_for_deletion.Count>=3)
                    {
                        no_cards_to_delete = 3;
                        canDraw = false;
                    }
                    break;
                case 3:
                    // Can't use it?
                    // remove 1 great resources tile from implementation section and 1 resource (any) tile from context section
                    // if plan contains expertise training, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==6) || (i==7) || (i==8) || (i==10) || (i==11) || (i==13) || (i==14) || (i==15) || (i==16) || (i==17) || (i==18) || (i==19)|| (i==22)) && card_colour_list[counter]=="red")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==8) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if (((i==1))&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    counter = 0;
                    foreach(int i in held_event_card_index)
                    {
                        if (i==1)
                        {
                            exceptions = true;
                        }
                    }
                    if ((cards_for_deletion.Count>=2)&&(exceptions==false))
                    {
                        no_cards_to_delete = 2;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 4:
                    // Competition for resources
                    // remove 1 results or great results tile from current section
                    // if context contains great resources, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==9) || (i==12) || (i==13)) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 5: case 6:
                    // Distraction
                    // Remove any 2 tiles from current section
                    // if plan contains milestones only remove 1 tile
                    distraction(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 7:
                    // Tried to do too much?
                    // remove any 2 tiles from the current section
                    // if context contains refine research questions, ignore effect
                    tried_to_do_too_much(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 8: case 9:
                    // Ethics!
                    // remove any 4 tiles from current section
                    // if plan contains ethical clearence, ignore effect
                    ethics(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 10:
                    // fire!
                    // remove 3 data, results, analysis, or interpretation tiles from the current section
                    foreach(int i in card_index_list)
                    {
                        if (((i==0) || (i==1) || (i==2) || (i==3) || (i==4) || (i==5) || (i==6) || (i==7) || (i==12) || (i==13)) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=3)&&(exceptions==false))
                    {
                        no_cards_to_delete = 3;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 11:
                    // flood!
                    // remove 1 great resources tiles and 3 results or great results tiles from the current section
                    foreach(int i in card_index_list)
                    {
                        if (((i==8) || (i==9) || (i==12) || (i==13)) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=4)&&(exceptions==false))
                    {
                        no_cards_to_delete = 4;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 12:
                    // lost enthusiasm
                    // remove any 1 tile from current section
                    // if plan contains holiday time, ignore effect
                    lost_enthusiasm(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 13:
                    // metadata fail!
                    // remove 2 interpretation tiles from current section
                    // if plan contains research data management stragegy, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==10) || (i==11)) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if (((i==5) || (i==6))&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=2)&&(exceptions==false))
                    {
                        no_cards_to_delete = 2;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 14:
                    // philosophy?
                    // remove 1 results tile and 1 interpretation tile from the current section
                    // if plan contains expertise training, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if (((i==10) || (i==11) || (i==12) || (i==13)) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==1)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    counter = 0;
                    foreach(int i in held_event_card_index)
                    {
                        if (i==1)
                        {
                            exceptions = true;
                        }
                    }
                    if ((cards_for_deletion.Count>=2)&&(exceptions==false))
                    {
                        no_cards_to_delete = 2;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 15:
                    // procrastinare!
                    // remove any 3 tiles from the current section
                    // if plan contains milestones ignore effect, but turn over a milestones tile as your deadlines go sailing by
                    procrastinate(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 16:
                    // real world relevance
                    // if context contains resource (policy) you may add an interpretation tile to the implementation section
                    foreach(int i in card_index_list)
                    {
                        if (((i==8) || (i==17) || (i==22))&&(card_colour_list[counter]=="red"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    if (exceptions==false)
                    {
                        object_to_spawn = GameObject.Find("BACK-act-imp-01");
                        object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 10);
                    }
                    break;
                case 17:
                    // resources...
                    // remove 1 resource (any) tile from context section
                    // if context contains discussion with exterts, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if ((i==8) && card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==0)&&(card_colour_list[counter]=="red"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 18:
                    // sick!
                    // remove any 2 tiles from the current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    sick(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
                case 19:
                    // sister's wedding
                    // remove any 1 tiles from current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    foreach(int i in card_index_list)
                    {
                        if (card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                            cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    if ((cards_for_turnover.Count>=1)&&(exceptions==true))
                    {
                        no_cards_to_turnover = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_turnover.Clear();
                        no_cards_to_turnover = 0;
                    }
                    break;
                case 20:
                    // statistics!
                    // remove 1 results tile and 1 analysis tile from the current section
                    // if plan contains expertise training, ignore effect
                    foreach(int i in card_index_list)
                    {
                        if (card_colour_list[counter]=="green")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==4)&&(card_colour_list[counter]=="yellow"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    counter = 0;
                    foreach(int i in held_event_card_index)
                    {
                        if (i==1)
                        {
                            exceptions = true;
                        }
                    }
                    if ((cards_for_deletion.Count>=1)&&(exceptions==false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 21:
                    // supervisor gets sick
                    // block out 2 spaces in the write up section
                    // if plan contains milestones ignore effect
                    foreach(int i in card_index_list)
                        {
                            if ((i==3)&&(card_colour_list[counter]=="yellow"))
                            {
                                exceptions = true;
                            }
                            counter += 1;
                        }
                        if (exceptions==false)
                        {
                            object_to_spawn = GameObject.Find("BACK-act-write-up-01");
                            object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                            object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 13);
                        }
                        else {
                            cards_for_deletion.Clear();
                            no_cards_to_delete = 0;
                        }
                        break;
                case 22:
                    // who needs theory?
                    // remove 2 interpretation or analysis tiles from the current section
                    // if context contains 3 or more relevant or very relevant article tiles, ignore effect
                    int exceptionCounter = 0;
                    foreach(int i in card_index_list)
                        {
                            if (((i==0) || (i==1) || (i==2) || (i==3) || (i==10) || (i==11)) && (card_colour_list[counter]=="green"))
                            {
                                position_of_cards_on_board.Add(counter);
                                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            }
                            if (((i==3) || (i==20) || (i==21))&&(card_colour_list[counter]=="red"))
                            {
                                exceptionCounter += 1;
                                if (exceptionCounter>=3)
                                {
                                    exceptions = true;
                                }
                            }
                            counter += 1;
                        }
                        if ((cards_for_deletion.Count>=2)&&(exceptions==false))
                        {
                            no_cards_to_delete = 2;
                            canDraw = false;
                        }
                        else {
                            cards_for_deletion.Clear();
                            no_cards_to_delete = 0;
                        }
                        break;
                case 23:
                    // tunnel vision
                    // remove any 1 tile from current section
                    // if plan contains meetings with supervisor or context contains discussion with experts or refine research questions, ignore effect
                    tunnel_vision(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "green");
                    break;
            }
        }

        if (event_card_colour=="blue")
        {
            switch (rand_num)
            {
                case 0:
                    // bibliography?
                    // remove any 1 thesis tile from current section
                    // if context contains reference management system, ignore effect
                    bibliography(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions);
                    break;
                case 1:
                    // Bluescreen!
                    // Remove any 3 tiles from the current section
                    // if plan contains research data management strategy, ignore effect
                    bluescreen(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 2:
                    // brother's wedding
                    // remove 1 thesis tile from current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && is_thesis(i))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i == 4) && (card_colour_list[counter] == "yellow"))
                        {
                            exceptions = true;
                            cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    if ((cards_for_turnover.Count>=1)&&(exceptions==true))
                    {
                        no_cards_to_turnover = 1;
                        canDraw = false;
                    }
                    else {
                        cards_for_turnover.Clear();
                        no_cards_to_turnover = 0;
                    }
                    break;
                case 3:
                    // disagreement
                    // remove 2 sound conclusions or conclusions tiles from the current section
                    // if context contains at least 1 very relevant article and at least 1 very relevant methodology, ignore effect
                    bool contains_very_relevant_article = false;
                    bool contains_very_relevant_methodology = false;
                    int conclusion_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && (i == 0 || i == 1 || i == 11))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            conclusion_count += 1;
                        }
                        if ((i == 3) && (card_colour_list[counter] == "red"))
                        {
                            contains_very_relevant_article = true;
                        }
                        if ((i == 2) && (card_colour_list[counter] == "red"))
                        {
                            contains_very_relevant_methodology = true;
                        }
                        counter += 1;
                    }
                    if (contains_very_relevant_article && contains_very_relevant_methodology)
                    {
                        exceptions = true;
                    }
                    if (conclusion_count > 2)
                    {
                        conclusion_count = 2;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = conclusion_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 4: case 5:
                    // Distraction
                    // Remove any 2 tiles from current section
                    // if plan contains milestones only remove 1 tile
                    distraction(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 6:
                    // does it work?
                    // remove any 3 tiles from current section
                    // if write-up contains at least 1 evaluation tile, ignore effect
                    int tile_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue")
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 6) && (card_colour_list[counter] == "blue"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    if (tile_count > 3)
                    {
                        tile_count = 3;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 7: case 8: case 9:
                    // Tried to do too much?
                    // remove any 2 tiles from the current section
                    // if context contains refine research questions, ignore effect
                    tried_to_do_too_much(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 10:
                    // ethics
                    // remove any 4 tiles from current section
                    // if plan contains ethical clearence, ignore effect
                    ethics(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 11:
                    // fit for purpose
                    // remove 2 non-adjacent thesis tiles from current section
                    // if context contains 2 or more methodology or very relevant methodology tiles, ignore effect.
                    tile_count = 0;
                    int exception_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && is_thesis(i))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 9 || i == 12 || i == 2) && (card_colour_list[counter] == "red"))
                        {
                            exception_count += 1;
                        }
                        counter += 1;
                    }
                    if (exception_count >= 2)
                    {
                        exceptions = true;
                    }
                    if (tile_count > 2)
                    {
                        tile_count = 2;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 12:
                    // jargon
                    // remove 2 non-adjacent explanation tiles from current section
                    // if write-up contains support from family/friends or you have a proofreader card, ignore effect.
                    foreach (int i in card_index_list)
                    {
                        if (((i==5) || (i==6) || (i==7)) && (card_colour_list[counter] == "blue"))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        if ((i==8) && (card_colour_list[counter] == "blue"))
                        {
                            exceptions = true;
                        }
                        counter += 1;
                    }
                    // use and remove held event card if it exists in your deck
                    counter = 0;
                    foreach(int i in held_event_card_index)
                    {
                        if (i==3)
                        {
                            exceptions = true;
                            GameObject card = held_event_cards[counter];
                            SpawnEventCard.held_event_cards.RemoveAt(counter);
                            SpawnEventCard.held_event_card_index.RemoveAt(counter);
                            Destroy(card);
                            break;
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count >= 2) && (exceptions == false))
                    {
                        no_cards_to_delete = 2;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 13: case 14:
                    // lost enthusiasm
                    // remove any 1 tile from current section
                    // if plan contains holiday time, ignore effect
                    lost_enthusiasm(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 15:
                    // lost voice
                    // remove 2 non-adjacent critical writing or explanation tiles from current section
                    // if implementation contains 2 or more interpretation tiles, ignore effect
                    tile_count = 0;
                    exception_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && (i == 2 || i == 7 || i == 8 || i == 9))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 10 || i == 11) && (card_colour_list[counter] == "green"))
                        {
                            exception_count += 1;
                        }
                        counter += 1;
                    }
                    if (exception_count >= 2)
                    {
                        exceptions = true;
                    }
                    if (tile_count > 2)
                    {
                        tile_count = 2;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 16:
                    // procrastinate!
                    // remove any 3 tiles from the current section
                    // if plan contains milestones ignore effect, but turn over a milestones tile as your deadlines go sailing by
                    procrastinate(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 17:
                    // Held Card

                    // proofreader
                    // add an explanation tile to the write-up section and keep this card to protect against any future jargon events
                    object_to_spawn = GameObject.Find("BACK-act-write-up-01");
                    object_to_spawn.GetComponent<SpawnCard>().call_spawnCard(object_to_spawn, true, 5);

                    object_to_spawn = GameObject.Find("Player_Held_Event_Card_Spawner");
                    object_to_spawn.GetComponent<SpawnEventCard>().spawnEventCard(object_to_spawn, true, 17);
                    held_event_card_index.Add(3);
                    break;

                case 18:
                    // references?
                    // remove any 1 thesis tile from the current section
                    // if context contains reference management system, ignore, effect
                    bibliography(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions);
                    break;
                case 19:
                    // regurgitation
                    // remove 3 non-adjacent thesis tiles from current section
                    // if implementation contains 3 or more analysis or interpretation tiles, ignore effect
                    tile_count = 0;
                    exception_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && is_thesis(i))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 0 || i == 1 || i == 2 || i == 3 || i == 10 || i == 11) && (card_colour_list[counter] == "green"))
                        {
                            exception_count += 1;
                        }
                        counter += 1;
                    }
                    if (exception_count >= 3)
                    {
                        exceptions = true;
                    }
                    if (tile_count > 3)
                    {
                        tile_count = 3;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 20:
                    // sick!
                    // remove any 2 tiles from the current section
                    // if plan contains contingency time, turn over that tile and ignore this effect
                    sick(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 21:
                    // so what?
                    // remove 2 non-adjacent thesis tiles from current section
                    // if context contains 2 or more very relevant article tiles, ignore effect
                    tile_count = 0;
                    exception_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && is_thesis(i))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 3) && (card_colour_list[counter] == "red"))
                        {
                            exception_count += 1;
                        }
                        counter += 1;
                    }
                    if (exception_count >= 2)
                    {
                        exceptions = true;
                    }
                    if (tile_count > 2)
                    {
                        tile_count = 2;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 22:
                    // tunnel vision
                    // remove any 1 tile from current section
                    // if plan contains meetings with supervisor or context contains discussion with experts or refine research questions, ignore effect
                    tunnel_vision(card_index_list, card_colour_list, position_of_cards_on_board, counter, exceptions, "blue");
                    break;
                case 23:
                    // uncritical writing!
                    // remove 2 non-adjacent thesis tiles from current section
                    // if context contains 3 or more relevant or very relevant article tiles, ignore effect
                    tile_count = 0;
                    exception_count = 0;
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && is_thesis(i))
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                            tile_count += 1;
                        }
                        if ((i == 3 || i == 20 || i == 21) && (card_colour_list[counter] == "red"))
                        {
                            exception_count += 1;
                        }
                        counter += 1;
                    }
                    if (exception_count >= 3)
                    {
                        exceptions = true;
                    }
                    if (tile_count > 2)
                    {
                        tile_count = 2;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = tile_count;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
                case 24:
                    // waffle!
                    // remove a critical writing tile from the current section
                    foreach (int i in card_index_list)
                    {
                        if (card_colour_list[counter] == "blue" && i == 2)
                        {
                            position_of_cards_on_board.Add(counter);
                            cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                        }
                        counter += 1;
                    }
                    if ((cards_for_deletion.Count >= 1) && (exceptions == false))
                    {
                        no_cards_to_delete = 1;
                        canDraw = false;
                    }
                    else
                    {
                        cards_for_deletion.Clear();
                        no_cards_to_delete = 0;
                    }
                    break;
            }
        }
    }

    /* 
        Functions for cards which are repeated in multiple event rounds
    */
    private void bluescreen(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // Bluescreen!
        // Remove any 3 tiles from the current section
        // if plan contains research data management strategy, ignore effect
        foreach(int i in card_index_list)
            {
                if (card_colour_list[counter]==colour)
                {
                    position_of_cards_on_board.Add(counter);
                    cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                }
                if (((i==5)||(i==6))&&(card_colour_list[counter]=="yellow"))
                {
                    exceptions = true;
                }
                counter += 1;
            }
            if ((cards_for_deletion.Count>=3)&&(exceptions==false))
            {
                no_cards_to_delete = 3;
                canDraw = false;
            }
            else {
                cards_for_deletion.Clear();
                no_cards_to_delete = 0;
            }
    }

    private void distraction(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // Distraction
        // Remove any 2 tiles from current section
        // if plan contains milestones only remove 1 tile
        foreach(int i in card_index_list)
        {
            if (card_colour_list[counter]==colour)
            {
                position_of_cards_on_board.Add(counter);
                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
            }
            if ((i==3)&&(card_colour_list[counter]=="yellow"))
            {
                exceptions = true;
            }
            counter += 1;
        }
        if ((cards_for_deletion.Count>=2)&&(exceptions==false))
        {
            no_cards_to_delete = 2;
            canDraw = false;
        }
        else if ((cards_for_deletion.Count>=1)&&(exceptions==true))
        {
            no_cards_to_delete = 1;
            canDraw = false;
        }
        else {
            cards_for_deletion.Clear();
            no_cards_to_delete = 0;
        }
    }

    private void tried_to_do_too_much(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // Tried to do too much?
        // remove any 2 tiles from the current section
        // if context contains refine research questions, ignore effect
        foreach(int i in card_index_list)
        {
            if (card_colour_list[counter]==colour)
            {
                position_of_cards_on_board.Add(counter);
                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
            }
            if ((i==24)&&(card_colour_list[counter]=="red"))
            {
                exceptions = true;
            }
            counter += 1;
        }
        if ((cards_for_deletion.Count>=2)&&(exceptions==false))
        {
            no_cards_to_delete = 2;
            canDraw = false;
        }
        else {
            cards_for_deletion.Clear();
            no_cards_to_delete = 0;
        }
    }

    private void ethics(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // ethics
        // remove any 4 tiles from current section
        // if plan contains ethical clearence, ignore effect
        foreach(int i in card_index_list)
            {
                if (card_colour_list[counter]==colour)
                {
                    position_of_cards_on_board.Add(counter);
                    cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                }
                if ((i==0)&&(card_colour_list[counter]=="yellow"))
                {
                    exceptions = true;
                }
                counter += 1;
            }
            if ((cards_for_deletion.Count>=4)&&(exceptions==false))
            {
                no_cards_to_delete = 4;
                canDraw = false;
            }
            else {
                cards_for_deletion.Clear();
                no_cards_to_delete = 0;
            }
    }

    private void lost_enthusiasm(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // lost enthusiasm
        // remove any 1 tile from current section
        // if plan contains holiday time, ignore effect
        foreach(int i in card_index_list)
        {
            if (card_colour_list[counter]==colour)
            {
                position_of_cards_on_board.Add(counter);
                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
            }
            if ((i==2)&&(card_colour_list[counter]=="yellow"))
            {
                exceptions = true;
            }
            counter += 1;
        }
        if ((cards_for_deletion.Count>=1)&&(exceptions==false))
        {
            no_cards_to_delete = 1;
            canDraw = false;
        }
        else {
            cards_for_deletion.Clear();
            no_cards_to_delete = 0;
        }
    }

    private void procrastinate(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // procrastinate!
        // remove any 3 tiles from the current section
        // if plan contains milestones ignore effect, but turn over a milestones tile as your deadlines go sailing by
        foreach(int i in card_index_list)
            {
                if (card_colour_list[counter]==colour)
                {
                    position_of_cards_on_board.Add(counter);
                    cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                }
                if ((i==3)&&(card_colour_list[counter]=="yellow"))
                {
                    exceptions = true;
                    cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                }
                counter += 1;
            }
            if ((cards_for_deletion.Count>=3)&&(exceptions==false))
            {
                no_cards_to_delete = 3;
                canDraw = false;
            }
            else {
                cards_for_deletion.Clear();
                no_cards_to_delete = 0;
            }
            if ((cards_for_turnover.Count>=1)&&(exceptions==true))
            {
                no_cards_to_turnover = 1;
                canDraw = false;
            }
            else {
                cards_for_turnover.Clear();
                no_cards_to_turnover = 0;
            }
    }

    private void sick(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // sick!
        // remove any 2 tiles from the current section
        // if plan contains contingency time, turn over that tile and ignore this effect
        foreach(int i in card_index_list)
            {
                if (card_colour_list[counter]==colour)
                {
                    position_of_cards_on_board.Add(counter);
                    cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                }
                if ((i==4)&&(card_colour_list[counter]=="yellow"))
                {
                    exceptions = true;
                    cards_for_turnover.Add(SpawnCard.GetList()[counter]);
                }
                counter += 1;
            }
            if ((cards_for_deletion.Count>=2)&&(exceptions==false))
            {
                no_cards_to_delete = 2;
                canDraw = false;
            }
            else {
                cards_for_deletion.Clear();
                no_cards_to_delete = 0;
            }
            if ((cards_for_turnover.Count>=1)&&(exceptions==true))
            {
                no_cards_to_turnover = 1;
                canDraw = false;
            }
            else {
                cards_for_turnover.Clear();
                no_cards_to_turnover = 0;
            }
    }

    private void tunnel_vision(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions, string colour) {
        // tunnel vision
        // remove any 1 tile from current section
        // if plan contains meetings with supervisor or context contains discussion with experts or refine research questions, ignore effect
        foreach(int i in card_index_list)
            {
                if (card_colour_list[counter]==colour)
                {
                    position_of_cards_on_board.Add(counter);
                    cards_for_deletion.Add(SpawnCard.GetList()[counter]);
                }
                if (((i==9)&&(card_colour_list[counter]=="yellow")) || (((i==0)||(i==24))&&(card_colour_list[counter]=="red")))
                {
                    exceptions = true;
                }
                counter += 1;
            }
            //use held event card if exists and remove it from your deck
            counter = 0;
            foreach(int i in held_event_card_index)
            {
                if (i==2)
                {
                    exceptions = true;
                    GameObject card = held_event_cards[counter];
                    SpawnEventCard.held_event_cards.RemoveAt(counter);
                    SpawnEventCard.held_event_card_index.RemoveAt(counter);
                    Destroy(card);
                    break;
                }
                counter += 1;
            }
            if ((cards_for_deletion.Count>=1)&&(exceptions==false))
            {
                no_cards_to_delete = 1;
                canDraw = false;
            }
            else {
                cards_for_deletion.Clear();
                no_cards_to_delete = 0;
            }
    }

    private bool is_thesis(int i)
    {
        if (i == 0 || i == 1 || i == 2 || i == 7 || i == 8 || i == 9 || i == 11)
        {
            return true;
        }
        return false;
    }

    private void bibliography(List<int> card_index_list, List<string> card_colour_list, List<int> position_of_cards_on_board, int counter, bool exceptions)
    {
        // remove any 1 thesis tile from the current section
        // if context contains reference management system, ignore, effect
        foreach (int i in card_index_list)
        {
            if (card_colour_list[counter] == "blue" && is_thesis(i))
            {
                position_of_cards_on_board.Add(counter);
                cards_for_deletion.Add(SpawnCard.GetList()[counter]);
            }
            if ((i == 23) && (card_colour_list[counter] == "red"))
            {
                exceptions = true;
            }
            counter += 1;
        }
        if ((cards_for_deletion.Count >= 1) && (exceptions == false))
        {
            no_cards_to_delete = 1;
            canDraw = false;
        }
        else
        {
            cards_for_deletion.Clear();
            no_cards_to_delete = 0;
        }
    }
}
