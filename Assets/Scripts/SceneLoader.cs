using System.Collections.Generic;
using UnityEngine.SceneManagement;

public static class SceneLoader
{
    private readonly static Dictionary<EnumGameRound, string> SceneNameDictionary = new Dictionary<EnumGameRound, string>
    {
        { EnumGameRound.MAIN_MENU, "Main Menu" },
        { EnumGameRound.WAITING_LOBBY, "Waiting Room" },
        { EnumGameRound.PLAN_ACTIVITY, "Build View"},
        { EnumGameRound.CONTEXT_ACTIVITY, "Build View"},
        { EnumGameRound.IMP_ACTIVITY, "Build View"},
        { EnumGameRound.WRITEUP_ACTIVITY, "Build View"},
        { EnumGameRound.END, "Build View"},
        { EnumGameRound.CONTEXT_EVENT, "Event View Round 1" },
        { EnumGameRound.IMP_EVENT, "Event View Round 2" },
        { EnumGameRound.WRITEUP_EVENT, "Event View Round 3" },
    };

    public static void Load(EnumGameRound round)
    {
        SceneManager.LoadScene(SceneNameDictionary[round]);
    }
}
