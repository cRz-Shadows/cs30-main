using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager INSTANCE { get; private set; }
    public RoundManager SceneManager;
    public NetworkManager NetworkManager;

    void Awake()
    {
        if (INSTANCE == null)
        {
            INSTANCE = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
        NetworkManager = new NetworkManager(SceneManager);
        Debug.Log("Game System Completed Setup Process");
    }
    void Start()
    {
        
    }
    void Update()
    {
        
    }
}
