using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SpawnCard : MonoBehaviour
{

    public GameObject ActivityCard;
    public Sprite[] ActivitySprites;
    public bool Active;
    private static List<GameObject> CreatedActivityCards = new List<GameObject>();
    private static List<int> CreatedActivityCards_Index = new List<int>();
    private static List<string> CreatedActivityCards_Colour = new List<string>();

    public static List<GameObject> GetList()
    {
        return CreatedActivityCards;
    }

    public static void removeObject(int index)
    {
        CreatedActivityCards.RemoveAt(index);
    }

    public static List<int> GetList_index()
    {
        return CreatedActivityCards_Index;
    }

    public static void removeIndex(int index)
    {
        CreatedActivityCards_Index.RemoveAt(index);
    }

        public static List<string> GetList_colour()
    {
        return CreatedActivityCards_Colour;
    }
        public static void removeColour(int index)
    {
        CreatedActivityCards_Colour.RemoveAt(index);
    }

    void OnMouseDown()
    {
        spawnCard(this.gameObject);
    }

    public void call_spawnCard(GameObject currentObject, bool spawnSpecific=false, int spawnNumber = -1)
    {
        Active = true;
        this.spawnCard(currentObject, spawnSpecific, spawnNumber);
    }

    public void spawnCard(GameObject currentObject, bool spawnSpecific=false, int spawnNumber = -1)
    {
        Debug.Log("Mouse Down");
        Debug.Log(Active + ":" + SpawnEventCard.canDraw);
        if (Active)
        {
            if (SpawnEventCard.canDraw)
            {
                Debug.Log("Mouse Down");
                int rand_num = Random.Range(0, ActivitySprites.Length - 1);
                if (spawnSpecific == true)
                {
                    rand_num = spawnNumber;
                }
                GameObject newActivityCard = GameManager.INSTANCE.SceneManager.SpawnCard(ActivitySprites[rand_num], transform.position.x, transform.position.y);
                SpriteRenderer cardSpriteRenderer = newActivityCard.GetComponent<SpriteRenderer>();
                CDrawActivityPacket packet = new CDrawActivityPacket(newActivityCard.GetInstanceID(), cardSpriteRenderer.sprite.name, transform.position.x, transform.position.y);
                GameManager.INSTANCE.NetworkManager.SendPacket(packet);
                CreatedActivityCards.Add(newActivityCard);
                CreatedActivityCards_Index.Add(rand_num);

                if (currentObject == GameObject.Find("BACK-act-plan-01"))
                {
                    CreatedActivityCards_Colour.Add("yellow");
                }
                if (currentObject == GameObject.Find("BACK-act-context-01"))
                {
                    CreatedActivityCards_Colour.Add("red");
                }
                if (currentObject == GameObject.Find("BACK-act-imp-01"))
                {
                    CreatedActivityCards_Colour.Add("green");
                }
                if (currentObject == GameObject.Find("BACK-act-write-up-01"))
                {
                    CreatedActivityCards_Colour.Add("blue");
                }

                newActivityCard.AddComponent<EventCardFunctions>();
            }
        }
    }
}
