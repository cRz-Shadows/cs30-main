using System;
public class TimerUtil
{
    private long ms;

    public long GetCurrent()
    {
        return DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
    }

    public long GetDifference()
    {
        return GetCurrent() - ms;
    }

    public bool HasReached(long ms)
    {
        return GetDifference() >= ms;
    }

    public void Reset()
    {
        ms = GetCurrent();
    }
}

