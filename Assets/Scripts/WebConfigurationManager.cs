using UnityEngine;
using System;
using System.Runtime.InteropServices;
using AOT;
using Newtonsoft.Json;
public class WebConfigurationManager : MonoBehaviour
{
    public static string IP_ADDRESS { get; set; }
    public static int ROUND_TIMER_MAX { get; set; }
#if UNITY_WEBGL && !UNITY_EDITOR
    public delegate void EnvironmentConfigurationCallback(System.IntPtr ptr);

    [DllImport("__Internal")]
    private static extern void GetEnvironmentConfiguration(EnvironmentConfigurationCallback callback);

    void Start()
    {
        GetEnvironmentConfiguration(Callback);
    }

    [MonoPInvokeCallback(typeof(EnvironmentConfigurationCallback))]
    public static void Callback(System.IntPtr ptr)
    {
        try
        {
            string value = Marshal.PtrToStringAuto(ptr);
            Config config = JsonConvert.DeserializeObject<Config>(value);
            IP_ADDRESS = config.IP;
            ROUND_TIMER_MAX = config.RoundTimerMax;
            GameManager.INSTANCE.NetworkManager.OpenConnection();
        } catch (Exception e)
        {
            Debug.LogError("Failed to read config file: " + e.Message);
        }
    }
#else
    void Start()
    {
        GetEnvironmentConfiguration();
    }

    private void GetEnvironmentConfiguration()
    {
        Config config = new Config("localhost", 60);
        IP_ADDRESS = config.IP;
        ROUND_TIMER_MAX = config.RoundTimerMax;
        GameManager.INSTANCE.NetworkManager.OpenConnection();
    }
#endif
}