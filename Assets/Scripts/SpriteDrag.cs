using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SpriteDrag : MonoBehaviour
{
    public int ParentObjectID;
    private bool isDragging;
    private Vector3 offset;

    void OnMouseDown()
    {
        isDragging = true;
        offset = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
    }

    void OnMouseUp()
    {
        isDragging = false;
        GameManager.INSTANCE.NetworkManager.SendPacket(new CMoveActivityPacket(ParentObjectID, transform.position.x, transform.position.y));

        //Destroy function
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (200 < mousePos.x & mousePos.x < 310 & 270 < mousePos.y & mousePos.y < 355)
        {
            int counter = 0;
            foreach(GameObject j in SpawnCard.GetList())
                {
                    if (j==this.gameObject)
                    {
                        break;
                    }
                    counter += 1;
                }
            SpawnCard.removeObject(counter);
            SpawnCard.removeIndex(counter);
            SpawnCard.removeColour(counter);
            Destroy(gameObject);
            GameManager.INSTANCE.NetworkManager.SendPacket(new CUpdateActivityPacket(ParentObjectID, true, null));
        }
    }

    void Update()
    {
        if (isDragging)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 newPos = mousePos - offset;
            transform.position = new Vector3(newPos.x, newPos.y, transform.position.z);
        }
    }
}