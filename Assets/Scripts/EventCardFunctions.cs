using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class EventCardFunctions : MonoBehaviour
{
    List<int> card_index_list = SpawnCard.GetList_index();

    /*
        Function for deleting or turning over cards in cards_for_deletion or cards_for_turnover.
    */
    void OnMouseDown()
    {
        if (SpawnEventCard.canDraw == false)
        {
            if(SpawnEventCard.cards_for_deletion.Contains(this.gameObject))
            {   
                int counter1 = 0;
                int counter2 = 0;
                bool isEventCard = true;
                foreach(GameObject i in SpawnEventCard.cards_for_deletion)
                {
                    if (i==this.gameObject)
                    {
                        foreach(GameObject j in SpawnCard.GetList())
                        {
                            if (j==this.gameObject)
                            {
                                isEventCard = false;
                                break;
                            }
                            counter2 += 1;
                        }
                        break;
                    }
                    counter1 += 1;
                }
                
                if (isEventCard)
                {
                    counter2 = 0;
                    foreach(GameObject i in SpawnEventCard.held_event_cards)
                    {
                        if (i==this.gameObject)
                        {
                            SpawnEventCard.held_event_cards.RemoveAt(counter2);
                            SpawnEventCard.held_event_card_index.RemoveAt(counter2);
                        }
                        counter2 += 1;
                    }
                }
                else
                {
                    SpawnCard.removeObject(counter2);
                    SpawnCard.removeIndex(counter2);
                    SpawnCard.removeColour(counter2);
                }

                SpawnEventCard.cards_for_deletion.RemoveAt(counter1);
                //Destroy(this.gameObject);
                GameManager.INSTANCE.SceneManager.DeleteCard(GameManager.INSTANCE.SceneManager.OurBoardID, this.gameObject.GetInstanceID());
                GameManager.INSTANCE.NetworkManager.SendPacket(new CUpdateActivityPacket(gameObject.GetComponent<SpriteDrag>().ParentObjectID, true, null));
                SpawnEventCard.no_cards_to_delete -= 1;

                if (SpawnEventCard.no_cards_to_delete <= 0)
                {
                    SpawnEventCard.cards_for_deletion.Clear();
                    SpawnEventCard.canDraw = true;
                } 
            }
            if(SpawnEventCard.cards_for_turnover.Contains(this.gameObject))
            {   
                int counter1 = 0;
                int counter2 = 0;
                // Find index of current card in cards_for_turnover and SpawnCard.GetList()
                foreach(GameObject i in SpawnEventCard.cards_for_turnover)
                {
                    if (i==this.gameObject)
                    {
                        foreach(GameObject j in SpawnCard.GetList())
                        {
                            if (j==this.gameObject)
                            {
                                break;
                            }
                            counter2 += 1;
                        }
                        break;
                    }
                    counter1 += 1;
                }
                // remove from relevent lists
                string colour = SpawnCard.GetList_colour()[counter2];
                SpawnCard.removeObject(counter2);
                SpawnCard.removeIndex(counter2);
                SpawnCard.removeColour(counter2);
                SpawnEventCard.cards_for_turnover.RemoveAt(counter1);
                // change sprite to back of card
                SpriteRenderer cardSpriteRenderer = this.gameObject.GetComponent<SpriteRenderer>();
                if (colour == "yellow")
                {
                    changeSprite(cardSpriteRenderer, "BACK-act-plan-01", 9);
                }
                if (colour == "red")
                {
                    changeSprite(cardSpriteRenderer, "BACK-act-context-01", 25);
                }
                if (colour == "green")
                {
                    changeSprite(cardSpriteRenderer, "BACK-act-imp-01", 14);
                }
                if (colour == "blue")
                {
                    changeSprite(cardSpriteRenderer, "BACK-act-write-up-01", 13);
                }
                SpawnEventCard.no_cards_to_turnover -= 1;
                SpawnEventCard.no_cards_to_delete -= 1;
                if (SpawnEventCard.no_cards_to_turnover <= 0)
                {
                    SpawnEventCard.cards_for_turnover.Clear();
                } 
                if (SpawnEventCard.no_cards_to_delete <= 0)
                {
                    SpawnEventCard.cards_for_deletion.Clear();
                    SpawnEventCard.canDraw = true;
                } 
            }
        }

        /*
            Function which changes the sprite of an object on screen
        */
        void changeSprite(SpriteRenderer cardSpriteRenderer, string spawner, int num_in_list)
        {
            GameObject object_to_spawn = GameObject.Find(spawner);
            SpawnCard spawncard_sprites = object_to_spawn.GetComponent<SpawnCard>();
            cardSpriteRenderer.sprite = spawncard_sprites.ActivitySprites[num_in_list];

            GameManager.INSTANCE.NetworkManager.SendPacket(new CUpdateActivityPacket(this.gameObject.GetComponent<SpriteDrag>().ParentObjectID, false, cardSpriteRenderer.sprite.name));
        }
    }
}
