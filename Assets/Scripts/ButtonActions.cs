using UnityEngine;
using UnityEngine.UI;
using System;

public class ButtonActions : MonoBehaviour
{
    public InputField JoinCodeInput;

    public void JoinSession()
    {
        if (Int32.TryParse(JoinCodeInput.text, out int code))
        {
            GameManager.INSTANCE.NetworkManager.SendPacket(new CJoinSessionPacket(code));
        }
    }

    public void CreateSession()
    {
        GameManager.INSTANCE.NetworkManager.SendPacket(new CCreateSessionPacket());
    }

    public void StartSession()
    {
        GameManager.INSTANCE.NetworkManager.SendPacket(new CStartSessionPacket());
    }

    public void NextRound()
    {
        GameManager.INSTANCE.NetworkManager.SendPacket(new CNextRoundPacket());
    }

    public void NextPlayer()
    {
        GameManager.INSTANCE.SceneManager.NextPlayer();
    }
}
