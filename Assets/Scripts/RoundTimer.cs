using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoundTimer : MonoBehaviour
{

    public GameObject timerText;
    public int startingSeconds;
    public bool started = false;
    public bool finished = false;

    private float elapsedTime;

    string getTimerText()
    {
        int currentTimeSeconds = (int) Mathf.Max(0, startingSeconds - elapsedTime);

        return (currentTimeSeconds / 60).ToString("00") + ":" + (currentTimeSeconds % 60).ToString("00");
    }

    void Start()
    {
        timerText = GameObject.Find("Canvas/Timer text");
        if (timerText != null)
            timerText.GetComponent<Text>().text = getTimerText();
    }

    void Update()
    {
        if (!finished && started)
        {
            if (timerText != null)
                timerText.GetComponent<Text>().text = getTimerText();
            elapsedTime += Time.deltaTime;
            if (elapsedTime > startingSeconds)
            {
                finished = true;
            }
        }
    }

    public void StartTimer()
    {
        started = true;
    }

    public void ResetTimer()
    {
        elapsedTime = 0;
        finished = false;
    }
}
