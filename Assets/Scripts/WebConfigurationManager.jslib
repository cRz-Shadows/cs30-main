mergeInto(LibraryManager.library, {

  GetEnvironmentConfiguration: function (obj) {
    function getPtrFromString(str) {
      var buffer = _malloc(lengthBytesUTF8(str) + 1);
      writeStringToMemory(str, buffer);
      return buffer;
    }
	console.log("Config request");
    var request = new XMLHttpRequest();
    // load the web-config.json via web request
    request.open("GET", "./config.json", true);
    request.onreadystatechange = function () {
      if (request.readyState === 4 && request.status === 200) {
        var buffer = getPtrFromString(request.responseText);
        Runtime.dynCall('vi', obj, [buffer]);
      }
	  else {
		console.log("Failed to read config file!");
	  }
    };
    request.send();
  }

});