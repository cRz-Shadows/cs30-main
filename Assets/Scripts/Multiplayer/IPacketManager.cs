using System;
using System.Collections.Generic;
public abstract class IPacketManager
{
    private static readonly Dictionary<EnumPacketDirection, Dictionary<ushort, Type>> IDToPacketMap = new Dictionary<EnumPacketDirection, Dictionary<ushort, Type>>();
    private static readonly Dictionary<EnumPacketDirection, Dictionary<Type, ushort>> PacketToIDMap = new Dictionary<EnumPacketDirection, Dictionary<Type, ushort>>();
    private static ushort PacketCount = 1;

    static IPacketManager()
    {
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CHelloPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CCreateSessionPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CJoinSessionPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CStartSessionPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CDrawActivityPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CMoveActivityPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CNextRoundPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CDrawEventPacket));
        RegisterPacket(EnumPacketDirection.SERVERBOUND, typeof(CUpdateActivityPacket));

        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SHelloPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SJoinSessionPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SDrawActivityPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SMoveActivityPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SChangeRoundPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SDrawEventPacket));
        RegisterPacket(EnumPacketDirection.CLIENTBOUND, typeof(SUpdateActivityPacket));
    }

    protected static void RegisterPacket(EnumPacketDirection packetDirection, Type packet)
    {
        if (!IDToPacketMap.ContainsKey(packetDirection))
            IDToPacketMap.Add(packetDirection, new Dictionary<ushort, Type>());

        if (!PacketToIDMap.ContainsKey(packetDirection))
            PacketToIDMap.Add(packetDirection, new Dictionary<Type, ushort>());

        IDToPacketMap[packetDirection].Add(PacketCount, packet);
        PacketToIDMap[packetDirection].Add(packet, PacketCount);
        PacketCount++;
    }

    public static ushort GetPacketID(EnumPacketDirection direction, Type packet)
    {
        if (!PacketToIDMap.ContainsKey(direction)) return 0;
        if (!PacketToIDMap[direction].ContainsKey(packet)) return 0;

        return PacketToIDMap[direction][packet];
    }

    public static Type GetPacketType(EnumPacketDirection direction, ushort packetID)
    {
        if (!IDToPacketMap.ContainsKey(direction)) return typeof(Packet);
        if (!IDToPacketMap[direction].ContainsKey(packetID)) return typeof(Packet);

        return IDToPacketMap[direction][packetID];
    }

    public abstract void ProcessPacket(string data);

    public abstract void SendPacket(Packet packet);

    public abstract void CloseConnection();

    public abstract object Sender { get; }

    public abstract Session Session {get; set; }
}