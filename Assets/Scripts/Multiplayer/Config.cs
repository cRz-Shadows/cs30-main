public class Config
{
    public string IP { get; set; }
    public int RoundTimerMax { get; set; }

    public Config(string ip, int roundTimer)
    {
        this.IP = ip;
        this.RoundTimerMax = roundTimer;
    }
}
