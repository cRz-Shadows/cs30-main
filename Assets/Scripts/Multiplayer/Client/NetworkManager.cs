﻿using System;
using System.Text;
using UnityEngine;
using HybridWebSocket;

public class NetworkManager
{
    private WebSocket ws;
    private IPacketManager packetManager;
    private readonly RoundManager sceneManager;
    public NetworkManager(RoundManager sceneManager)
    {
        this.sceneManager = sceneManager;
    }
    public void OpenConnection()
    {
        ws = WebSocketFactory.CreateInstance(WebConfigurationManager.IP_ADDRESS);
        ws.OnOpen += () =>
        {
            packetManager.SendPacket(new CHelloPacket("hello server!"));
            Debug.Log("Sent hello");
        };

        ws.OnMessage += (byte[] bytes) =>
        {
            string data = Encoding.UTF8.GetString(bytes);
            Debug.Log("Data Received: " + data);
            try
            {
                packetManager.ProcessPacket(data);
            }
            catch (Exception exc)
            {
                Debug.Log("Encountered Exception: " + exc.Message);
            }

        };

        ws.OnError += (string errMsg) =>
        {
            Debug.Log("WS error: " + errMsg);
        };

        ws.OnClose += (WebSocketCloseCode code) =>
        {
            Debug.Log("Connection closed");
        };

        ws.Connect();
        packetManager = new ClientPacketManager(this);
    }

    public void SendPacket(Packet packet)
    {
        if (packetManager != null)
            packetManager.SendPacket(packet);
    }

    public void SendData(string data)
    {
        ws.Send(Encoding.UTF8.GetBytes(data));
    }

    public void EnqueueCardSpawn(SDrawActivityPacket packet)
    {
        sceneManager.EnqueueAction(() => sceneManager.SpawnCard(packet.BoardID, packet.CardID, packet.SpriteName, packet.X, packet.Y));
    }

    public void EnqueueCardMove(SMoveActivityPacket packet)
    {
        sceneManager.EnqueueAction(() =>
        {
            sceneManager.MoveCard(packet.boardID, packet.cardID, packet.X, packet.Y);
        });
    }

    public void EnqueueSceneSwitch(SChangeRoundPacket packet)
    {
        sceneManager.EnqueueAction(() =>
        {
            sceneManager.ChangeGameRound(packet.GameRound, packet.BoardID);
        });
    }

    public void EnqueueEventSpawn(SDrawEventPacket packet)
    {
        sceneManager.EnqueueAction(() => sceneManager.SpawnEventRoundCard(packet.SpriteName, packet.X, packet.Y));
    }

    public void EnqueueUpdateActivity(SUpdateActivityPacket packet)
    {
        if (packet.Delete)
        {
            sceneManager.EnqueueAction(() => sceneManager.DeleteCard(packet.BoardID, packet.CardID));
        }
        else
        {
            sceneManager.EnqueueAction(() => sceneManager.UpdateSprite(packet.BoardID, packet.CardID, packet.NewSpriteName));
        }
    }
}
