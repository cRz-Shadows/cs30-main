using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using UnityEngine;

class ClientPacketManager : IPacketManager
{
    private static readonly Dictionary<ushort, Action<IPacketManager, string>> PacketHandlers = new Dictionary<ushort, Action<IPacketManager, string>>();
    private readonly NetworkManager networkManager;

    public override Session Session { get; set; }
    public override object Sender => networkManager;

    static ClientPacketManager() {
        RegisterPacketHandler(typeof(SHelloPacket), HandleSHelloPacket);
        RegisterPacketHandler(typeof(SJoinSessionPacket), HandleSJoinSessionPacket);
        RegisterPacketHandler(typeof(SDrawActivityPacket), HandleSDrawActivityPacket);
        RegisterPacketHandler(typeof(SMoveActivityPacket), HandleSMoveActivityPacket);
        RegisterPacketHandler(typeof(SChangeRoundPacket), HandleSChangeRoundPacket);
        RegisterPacketHandler(typeof(SDrawEventPacket), HandleSDrawEventPacket);
        RegisterPacketHandler(typeof(SUpdateActivityPacket), HandleSUpdateActivityPacket);
    }

    public ClientPacketManager(NetworkManager networkManager)
    {
        this.networkManager = networkManager;
    }

    private static void RegisterPacketHandler(Type packet, Action<IPacketManager, string> handler)
    {
        ushort packetID = IPacketManager.GetPacketID(EnumPacketDirection.CLIENTBOUND, packet);
        if (packetID != 0)
            PacketHandlers.Add(packetID, handler);
    }

    public override void ProcessPacket(string data)
    {
        EmptyPacket packet = JsonConvert.DeserializeObject<EmptyPacket>(data);
        EnumPacketDirection packetDir = packet.PacketDirection;
        ushort packetID = packet.PacketID;

        if (PacketHandlers.ContainsKey(packetID))
        {
            PacketHandlers[packetID](this, data);
        }
        else
        {
            Debug.Log("Packet type not found");
        }
    }

    public override void SendPacket(Packet packet)
    {
        packet.PacketID = GetPacketID(packet.PacketDirection, packet.GetType());
        string data = JsonConvert.SerializeObject(packet);
        networkManager.SendData(data);
    }

    public override void CloseConnection()
    {

    }

    public static void HandleSHelloPacket(IPacketManager packetManager, string data)
    {
        SHelloPacket packet = JsonConvert.DeserializeObject<SHelloPacket>(data);
        Debug.Log("Received Hello: " + packet.Message);
    }

    public static void HandleSJoinSessionPacket(IPacketManager packetManager, string data)
    {
        SJoinSessionPacket packet = JsonConvert.DeserializeObject<SJoinSessionPacket>(data);
        Debug.Log("Joined session: " + packet.SessionID);
        packetManager.Session = new Session(packet.SessionID);
        GameManager.INSTANCE.SceneManager.SessionID = packet.SessionID;
        GameManager.INSTANCE.SceneManager.OurBoardID = packet.BoardID;
        if (packet.Host)
            GameManager.INSTANCE.SceneManager.CurrentBoardID = 0;
        else
            GameManager.INSTANCE.SceneManager.CurrentBoardID = packet.BoardID;
        GameManager.INSTANCE.SceneManager.Spectator = packet.Host;
    }

    public static void HandleSDrawActivityPacket(IPacketManager packetManager, string data)
    {
        SDrawActivityPacket packet = JsonConvert.DeserializeObject<SDrawActivityPacket>(data);
        NetworkManager manager = ((ClientPacketManager)packetManager).networkManager;
        manager.EnqueueCardSpawn(packet);
    }

    public static void HandleSMoveActivityPacket(IPacketManager packetManager, string data)
    {
        SMoveActivityPacket packet = JsonConvert.DeserializeObject<SMoveActivityPacket>(data);
        NetworkManager manager = ((ClientPacketManager)packetManager).networkManager;
        manager.EnqueueCardMove(packet);
    }

    public static void HandleSChangeRoundPacket(IPacketManager packetManager, string data)
    {
        SChangeRoundPacket packet = JsonConvert.DeserializeObject<SChangeRoundPacket>(data);
        NetworkManager manager = ((ClientPacketManager)packetManager).networkManager;
        Debug.Log("Change round");
        manager.EnqueueSceneSwitch(packet);
    }

    public static void HandleSDrawEventPacket(IPacketManager packetManager, string data)
    {
        SDrawEventPacket packet = JsonConvert.DeserializeObject<SDrawEventPacket>(data);
        NetworkManager manager = ((ClientPacketManager)packetManager).networkManager;
        manager.EnqueueEventSpawn(packet);
    }

    public static void HandleSUpdateActivityPacket(IPacketManager packetManager, string data)
    {
        SUpdateActivityPacket packet = JsonConvert.DeserializeObject<SUpdateActivityPacket>(data);
        NetworkManager manager = ((ClientPacketManager)packetManager).networkManager;
        manager.EnqueueUpdateActivity(packet);
    }

    class EmptyPacket
    {
        public EnumPacketDirection PacketDirection { get; set; }
        public ushort PacketID { get; set; }
    }
}