﻿public class SJoinSessionPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public int SessionID { get; set; }
    public bool Host { get; set; }

    public int BoardID { get; set; }

    public SJoinSessionPacket(int sessionID, int boardID, bool host)
    {
        this.SessionID = sessionID;
        this.Host = host;
        this.BoardID = boardID;
    }
}

