public abstract class Packet
{
    public abstract EnumPacketDirection PacketDirection { get; }
    public ushort PacketID { get; set; }
}
