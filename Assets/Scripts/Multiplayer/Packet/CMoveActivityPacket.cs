﻿public class CMoveActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public int CardID { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public CMoveActivityPacket(int cardID, float x, float y)
    {
        this.CardID = cardID;
        this.X = x;
        this.Y = y;
    }
}
