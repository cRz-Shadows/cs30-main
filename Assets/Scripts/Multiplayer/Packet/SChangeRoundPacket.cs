﻿public class SChangeRoundPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public EnumGameRound GameRound { get; set; }
    public int BoardID { get; set; }

    public SChangeRoundPacket(EnumGameRound gameRound, int boardID)
    {
        this.GameRound = gameRound;
        this.BoardID = boardID;
    }
}