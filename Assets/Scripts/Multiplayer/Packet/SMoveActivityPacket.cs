﻿public class SMoveActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public int boardID { get; set; }
    public int cardID { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public SMoveActivityPacket(int boardID, int cardID, float x, float y)
    {
        this.boardID = boardID;
        this.cardID = cardID;
        this.X = x;
        this.Y = y;
    }
}
