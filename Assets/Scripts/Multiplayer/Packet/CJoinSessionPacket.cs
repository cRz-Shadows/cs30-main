﻿public class CJoinSessionPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public int SessionID { get; set; }
    public CJoinSessionPacket(int sessionID)
    {
        this.SessionID = sessionID;
    }
}