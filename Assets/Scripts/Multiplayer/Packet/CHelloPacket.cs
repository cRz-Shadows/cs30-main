﻿public class CHelloPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public string Message { get; set; }

    public CHelloPacket(string message)
    {
        this.Message = message;
    }
}
