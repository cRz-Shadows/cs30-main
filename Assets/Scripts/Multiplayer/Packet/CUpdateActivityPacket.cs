
public class CUpdateActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public int CardID { get; set; }
    public string NewSpriteName { get; set; }
    public bool Delete { get; set; }
    public CUpdateActivityPacket(int cardID, bool delete, string name)
    {
        this.CardID = cardID;
        this.Delete = delete;
        this.NewSpriteName = name;
    }
}
