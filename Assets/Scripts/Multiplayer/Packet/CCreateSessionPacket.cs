﻿public class CCreateSessionPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;

    public CCreateSessionPacket()
    {

    }
}