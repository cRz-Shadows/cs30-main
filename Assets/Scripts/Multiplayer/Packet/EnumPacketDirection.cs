﻿public enum EnumPacketDirection : ushort
{
    SERVERBOUND,
    CLIENTBOUND
}
