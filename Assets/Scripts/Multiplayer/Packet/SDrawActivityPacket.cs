﻿public class SDrawActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;

    public int BoardID { get; set; }
    public int CardID { get; set; }
    public string SpriteName { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public SDrawActivityPacket(int boardID, int cardID, string spriteName, float x, float y)
    {
        this.BoardID = boardID;
        this.CardID = cardID;
        this.SpriteName = spriteName;
        this.X = x;
        this.Y = y;
    }
}