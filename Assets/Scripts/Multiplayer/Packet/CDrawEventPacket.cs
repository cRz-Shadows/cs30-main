public class CDrawEventPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public string SpriteName { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public CDrawEventPacket(string spriteName, float x, float y)
    {
        this.SpriteName = spriteName;
        this.X = x;
        this.Y = y;
    }
}
