public class SUpdateActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public int BoardID { get; set; }
    public int CardID { get; set; }
    public string NewSpriteName { get; set; }
    public bool Delete { get; set; }
    public SUpdateActivityPacket(int boardID, int cardID, bool delete, string name)
    {
        this.BoardID = boardID;
        this.NewSpriteName = name;
        this.CardID = cardID;
        this.Delete = delete;
    }
}
