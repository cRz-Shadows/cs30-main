﻿public class CDrawActivityPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.SERVERBOUND;
    public int CardID { get; set; }
    public string SpriteName { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public CDrawActivityPacket(int cardID, string spriteName, float x, float y)
    {
        this.CardID = cardID;
        this.SpriteName = spriteName;
        this.X = x;
        this.Y = y;
    }
}