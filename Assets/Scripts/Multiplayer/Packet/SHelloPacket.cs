﻿public class SHelloPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public string Message { get; set; }

    public SHelloPacket(string message)
    {
        this.Message = message;
    }
}
