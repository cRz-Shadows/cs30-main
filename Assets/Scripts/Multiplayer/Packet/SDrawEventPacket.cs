public class SDrawEventPacket : Packet
{
    public override EnumPacketDirection PacketDirection => EnumPacketDirection.CLIENTBOUND;
    public string SpriteName { get; set; }
    public float X { get; set; }
    public float Y { get; set; }

    public SDrawEventPacket(string spriteName, float x, float y)
    {
        this.SpriteName = spriteName;
        this.X = x;
        this.Y = y;
    }
}
