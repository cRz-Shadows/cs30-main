using System;
using System.Text;
using System.Threading;
using WebSocketSharp;
using WebSocketSharp.Server;
using Newtonsoft.Json;
class UnityWebServer : WebSocketBehavior
{
    private readonly IPacketManager packetManager;

    public UnityWebServer()
    {
        packetManager = new ServerPacketManager(this);
    }

    protected override void OnOpen()
    {
        Console.WriteLine("Client Connected!");
    }

    protected override void OnMessage(MessageEventArgs e)
    {
        string data = Encoding.UTF8.GetString(e.RawData);
        packetManager.ProcessPacket(data);
    }

    protected override void OnClose(CloseEventArgs e)
    {
        base.OnClose(e);
        if (packetManager.Session != null)
            packetManager.Session.OnCloseConnection(packetManager);
    }

    public void SendData(string data)
    {
        Send(data);
    }

    public void SendData(byte[] data)
    {
        Send(data);
    }

    public void Close()
    {
        this.Context.WebSocket.Close();
    }
}
class Server
{
    public static int MAX_ROUND_TIMER { get; set; }
    public class ServerTimer {
        public void TimerTick(Object state)
        {
            try
            {
                SessionManager.ServerTick();
            } catch (Exception e)
            {
                Console.WriteLine("Error during server tick: " + e.Message);
            }
        }
    }

    public static string TryReadConfig()
    {
        try
        {
            string configFile = System.IO.File.ReadAllText(System.IO.Directory.GetCurrentDirectory() + "\\config.json");
            Config config = JsonConvert.DeserializeObject<Config>(configFile);
            MAX_ROUND_TIMER = config.RoundTimerMax;
            return config.IP;
        }
        catch (Exception e)
        {
            Console.WriteLine("Unable to read config file. Please place a \"config\".json file in the path");
            Console.WriteLine(e.Message);
        }

        return null;
    }
    public static void Main(string[] args)
    {
        string IP = TryReadConfig();
        if (IP == null) return;
        var wssv = new WebSocketServer(IP);
        wssv.Start();
        wssv.AddWebSocketService<UnityWebServer>("/");
        Console.WriteLine("Started Server!");
        ServerTimer serverTimer = new ServerTimer();
        Timer timer = new Timer(serverTimer.TimerTick, null, 0, 1000 / 5); // Tick 5 times per second
        Console.ReadKey(true);
        timer.Dispose();
        wssv.Stop();
    }
}