﻿using System.Collections.Generic;
using System;

public class Session
{
    public readonly int ID;
    private readonly Dictionary<IPacketManager, Board> boards = new Dictionary<IPacketManager, Board>();
    private static readonly long SHUTDOWN_THRESHOLD = 60_000 * 5; // 5 minutes
    private readonly IPacketManager host;
    private readonly TimerUtil roundTimer;
    private readonly TimerUtil eventTimer;
    private EnumGameRound gameRound;
    private int currentBoard;
    private int playedEventCards;

    public Session(int id, IPacketManager host) : this(id)
    {
        this.host = host;
    }

    public Session(int id)
    {
        this.ID = id;
        this.gameRound = EnumGameRound.WAITING_LOBBY;
        this.roundTimer = new TimerUtil();
        this.eventTimer = new TimerUtil();
        this.eventTimer.Reset();
        this.currentBoard = 0;
    } 

    public void Tick()
    {
        long current = roundTimer.GetCurrent();
        if (gameRound.IsActivityRound() && roundTimer.HasReached(Server.MAX_ROUND_TIMER * 1000) && gameRound != EnumGameRound.PLAN_ACTIVITY)
        {
            NextGameRound();
        }

        if (eventTimer.HasReached(SHUTDOWN_THRESHOLD))
            CloseSession();
    }

    public bool Connect(IPacketManager sender)
    {
        if (boards.Count >= 6)
            return false;

        if (sender != host)
            boards.Add(sender, new Board(boards.Count));
        sender.SendPacket(new SChangeRoundPacket(gameRound, -1));
        eventTimer.Reset();
        return true;
    }

    public void OnCloseConnection(IPacketManager sender)
    {
        if (boards.ContainsKey(sender) || sender == host)
        {
            if (boards.ContainsKey(sender))
                boards.Remove(sender);
            if (boards.Count == 0)
                SessionManager.CloseSession(this);
        }
    }

    public void CloseSession()
    {
        try
        {
            foreach (var item in boards)
            {
                item.Key.CloseConnection();
            }
            host.CloseConnection();
        }
        catch (Exception) { }
    }

    public Board GetBoard(IPacketManager sender)
    {
        return boards[sender];
    }

    public void StartGame(IPacketManager sender)
    {
        if (sender == host)
        {
            ChangeGameRound(EnumGameRound.PLAN_ACTIVITY, -1);
        }
    }

    public void PlayActivityCard(IPacketManager sender, string name, int cardID, float cardX, float cardY)
    {
        if (boards.ContainsKey(sender))
        {
            Board board = boards[sender];
            board.AddCard(new Card(cardID, cardX, cardY));

            SendPacketToOthers(sender, new SDrawActivityPacket(GetBoard(sender).GetID(), cardID, name, cardX, cardY));
            this.eventTimer.Reset();
        }
    }

    public void MoveCard(IPacketManager sender, int cardID, float x, float y)
    {
        if (boards.ContainsKey(sender))
        { 
            Board board = boards[sender];
            SendPacketToOthers(sender, new SMoveActivityPacket(board.GetID(), cardID, x, y));
        }
    }

    public void NextGameRound(IPacketManager sender)
    {
        if (sender == host)
        {
            NextGameRound();
        }
    }

    public void PlayEventCard(IPacketManager sender, string name, float cardX, float cardY)
    {
        if (boards.ContainsKey(sender) && gameRound.IsEventRound() && GetBoard(sender).GetID() == currentBoard)
        {
            playedEventCards++;
            SendPacketToOthers(sender, new SDrawEventPacket(name, cardX, cardY));
        }
    }

    public void DeleteActivityCard(IPacketManager sender, int cardID)
    {
        Board board = GetBoard(sender);
        board.DeleteCardByID(cardID);
        SendPacketToOthers(sender, new SUpdateActivityPacket(board.GetID(), cardID, true, null));
    }

    public void ChangeActivitySprite(IPacketManager sender, int cardID, string sprite)
    {
        SendPacketToOthers(sender, new SUpdateActivityPacket(GetBoard(sender).GetID(), cardID, false, sprite));
    }

    private void NextGameRound()
    {
        EnumGameRound next = gameRound.NextRound() ?? EnumGameRound.WAITING_LOBBY;
        int boardID = -1;

        if (next.IsEventRound())
            boardID = 0;
        if (gameRound.IsEventRound() && currentBoard < boards.Count - 1)
        {
            boardID = currentBoard + 1;
            next = gameRound;
        }
        ChangeGameRound(next, boardID);
    }

    private void ChangeGameRound(EnumGameRound gameRound, int boardID)
    {
        roundTimer.Reset();
        this.gameRound = gameRound;
        this.currentBoard = boardID;
        this.playedEventCards = 0;
        SendPacketToEveryone(new SChangeRoundPacket(gameRound, boardID));
    }

    private void SendPacketToEveryone(Packet packet)
    {
        foreach (var item in boards)
        {
            item.Key.SendPacket(packet);
        }
        host.SendPacket(packet);
        this.eventTimer.Reset();
    }

    private void SendPacketToOthers(IPacketManager sender, Packet packet)
    {
        foreach (var item in boards)
        {
            if (item.Key != sender)
            {
                item.Key.SendPacket(packet);
            }
        }
        host.SendPacket(packet);
        this.eventTimer.Reset();
    }
}
