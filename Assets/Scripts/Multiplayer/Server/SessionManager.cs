﻿using System.Collections.Generic;
using System;
using System.IO;
public class SessionManager
{
    private static readonly int MIN_SESSION_ID = 10000;
    private static readonly int MAX_SESSION_ID = 99999;

    private static readonly Dictionary<int, Session> Sessions = new Dictionary<int, Session>();
    private static readonly Random RandomGenerator = new Random();

    public static void ServerTick()
    {
        foreach(var item in Sessions)
        {
            item.Value.Tick();
        }
    }

    public static Session CreateSession(IPacketManager sender)
    {
        int sessionID = GenerateSessionID();
        Session session = new Session(sessionID, sender);

        AddSession(session);
        session.Connect(sender);

        return session;
    }

    public static Session ConnectSession(IPacketManager sender, int sessionID)
    {
        if (!Sessions.ContainsKey(sessionID) || !Sessions[sessionID].Connect(sender))
            return null;

        return Sessions[sessionID];
    }

    public static void AddSession(Session session)
    {
        Sessions.Add(session.ID, session);
    }

    public static void CloseSession(Session session)
    {
        session.CloseSession();
        Sessions.Remove(session.ID);
    }

    private static int GenerateSessionID()
    {
        int sessionID;
        do
        {
            sessionID = RandomGenerator.Next(MIN_SESSION_ID, MAX_SESSION_ID);
        } while (Sessions.ContainsKey(sessionID));

        return sessionID;
    }
}
