﻿using System;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;

class ServerPacketManager : IPacketManager
{
    private static readonly Dictionary<ushort, Action<IPacketManager, string>> PacketHandlers = new Dictionary<ushort, Action<IPacketManager, string>>();
    private readonly UnityWebServer unityWebServer;

    public override Session Session { get; set; }
    public override object Sender => unityWebServer;

    static ServerPacketManager()
    {
        RegisterPacketHandler(typeof(CHelloPacket), HandleCHelloPacket);
        RegisterPacketHandler(typeof(CCreateSessionPacket), HandleCCreateSessionPacket);
        RegisterPacketHandler(typeof(CJoinSessionPacket), HandleCJoinSessionPacket);
        RegisterPacketHandler(typeof(CDrawActivityPacket), HandleCDrawActivityPacket);
        RegisterPacketHandler(typeof(CMoveActivityPacket), HandleCMoveActivityPacket);
        RegisterPacketHandler(typeof(CStartSessionPacket), HandleCStartSessionPacket);
        RegisterPacketHandler(typeof(CNextRoundPacket), HandleCNextRoundPacket);
        RegisterPacketHandler(typeof(CDrawEventPacket), HandleCDrawEventPacket);
        RegisterPacketHandler(typeof(CUpdateActivityPacket), HandleCUpdateActivityPacket);
    }

    public ServerPacketManager(UnityWebServer unityWebServer)
    {
        this.unityWebServer = unityWebServer;
    }

    private static void RegisterPacketHandler(Type packet, Action<IPacketManager, string> handler)
    {
        ushort packetID = IPacketManager.GetPacketID(EnumPacketDirection.SERVERBOUND, packet);
        if (packetID != 0)
            PacketHandlers.Add(packetID, handler);
    }

    public override void ProcessPacket(string data)
    {
        try
        {
            EmptyPacket packet = JsonConvert.DeserializeObject<EmptyPacket>(data);
            EnumPacketDirection packetDir = packet.PacketDirection;
            ushort packetID = packet.PacketID;

            if (PacketHandlers.ContainsKey(packetID))
            {
                PacketHandlers[packetID](this, data);
            }
        } catch (Exception e)
        {
            Console.WriteLine("Failed to read packet: " + data);
            Console.WriteLine(e.Message);
        }
        
    }

    public override void CloseConnection()
    {
        unityWebServer.Close();
    }

    public override void SendPacket(Packet packet)
    {
        packet.PacketID = GetPacketID(packet.PacketDirection, packet.GetType());
        string data = JsonConvert.SerializeObject(packet);
        unityWebServer.SendData(Encoding.UTF8.GetBytes(data));
    }

    public static void HandleCHelloPacket(IPacketManager packetManager, string data)
    {
        CHelloPacket packet = JsonConvert.DeserializeObject<CHelloPacket>(data);
        Console.WriteLine("Received Client Hello: " + packet.Message);
        packetManager.SendPacket(new SHelloPacket("Hello"));
    }

    public static void HandleCCreateSessionPacket(IPacketManager packetManager, string data)
    {
        CCreateSessionPacket packet = JsonConvert.DeserializeObject<CCreateSessionPacket>(data);
        Session session = SessionManager.CreateSession(packetManager);
        packetManager.Session = session;

        packetManager.SendPacket(new SJoinSessionPacket(session.ID, -1, true));
    }

    public static void HandleCJoinSessionPacket(IPacketManager packetManager, string data)
    {
        CJoinSessionPacket packet = JsonConvert.DeserializeObject<CJoinSessionPacket>(data);
        Session session = SessionManager.ConnectSession(packetManager, packet.SessionID);

        if (session != null)
        {
            packetManager.SendPacket(new SJoinSessionPacket(packet.SessionID, session.GetBoard(packetManager).GetID(), false));
            packetManager.Session = session;
            Console.WriteLine("Client joined session: " + session.ID);
        }
    }

    public static void HandleCStartSessionPacket(IPacketManager packetManager, string data)
    {
        CStartSessionPacket packet = JsonConvert.DeserializeObject<CStartSessionPacket>(data);
        packetManager.Session.StartGame(packetManager);
    }

    public static void HandleCDrawActivityPacket(IPacketManager packetManager, string data)
    {
        CDrawActivityPacket packet = JsonConvert.DeserializeObject<CDrawActivityPacket>(data);
        packetManager.Session.PlayActivityCard(packetManager, packet.SpriteName, packet.CardID, packet.X, packet.Y);
    }

    public static void HandleCMoveActivityPacket(IPacketManager packetManager, string data)
    {
        CMoveActivityPacket packet = JsonConvert.DeserializeObject<CMoveActivityPacket>(data);
        packetManager.Session.MoveCard(packetManager, packet.CardID, packet.X, packet.Y);
    }

    public static void HandleCNextRoundPacket(IPacketManager packetManager, string data)
    {
        CNextRoundPacket packet = JsonConvert.DeserializeObject<CNextRoundPacket>(data);
        packetManager.Session.NextGameRound(packetManager);
    }

    public static void HandleCDrawEventPacket(IPacketManager packetManager, string data)
    {
        CDrawEventPacket packet = JsonConvert.DeserializeObject<CDrawEventPacket>(data);
        packetManager.Session.PlayEventCard(packetManager, packet.SpriteName, packet.X, packet.Y);
    }

    public static void HandleCUpdateActivityPacket(IPacketManager packetManager, string data)
    {
        CUpdateActivityPacket packet = JsonConvert.DeserializeObject<CUpdateActivityPacket>(data);
        if (packet.Delete)
        {
            packetManager.Session.DeleteActivityCard(packetManager, packet.CardID);
        }
        else
        {
            packetManager.Session.ChangeActivitySprite(packetManager, packet.CardID, packet.NewSpriteName);
        }
    }

    class EmptyPacket
    {
        public EnumPacketDirection PacketDirection { get; set; }
        public ushort PacketID { get; set; }
    }
}
