﻿public enum EnumGameRound : ushort
{
    MAIN_MENU,
    WAITING_LOBBY,
    PLAN_ACTIVITY,
    CONTEXT_ACTIVITY,
    CONTEXT_EVENT,
    IMP_ACTIVITY,
    IMP_EVENT,
    WRITEUP_ACTIVITY,
    WRITEUP_EVENT,
    END,
}

public static class EnumGameRoundMethods
{
    public static EnumGameRound? NextRound(this EnumGameRound round)
    {
        return round switch
        {
            EnumGameRound.MAIN_MENU => EnumGameRound.WAITING_LOBBY,
            EnumGameRound.WAITING_LOBBY => EnumGameRound.PLAN_ACTIVITY,
            EnumGameRound.PLAN_ACTIVITY => EnumGameRound.CONTEXT_ACTIVITY,
            EnumGameRound.CONTEXT_ACTIVITY => EnumGameRound.CONTEXT_EVENT,
            EnumGameRound.CONTEXT_EVENT => EnumGameRound.IMP_ACTIVITY,
            EnumGameRound.IMP_ACTIVITY => EnumGameRound.IMP_EVENT,
            EnumGameRound.IMP_EVENT => EnumGameRound.WRITEUP_ACTIVITY,
            EnumGameRound.WRITEUP_ACTIVITY => EnumGameRound.WRITEUP_EVENT,
            EnumGameRound.WRITEUP_EVENT => EnumGameRound.END,
            _ => null,
        };
    }

    public static bool IsActivityRound(this EnumGameRound round)
    {
        return round == EnumGameRound.PLAN_ACTIVITY || round == EnumGameRound.CONTEXT_ACTIVITY || round == EnumGameRound.IMP_ACTIVITY || round == EnumGameRound.WRITEUP_ACTIVITY;
    }

    public static bool IsEventRound(this EnumGameRound round)
    {
        return round == EnumGameRound.CONTEXT_EVENT || round == EnumGameRound.IMP_EVENT || round == EnumGameRound.WRITEUP_EVENT;
    }
}
