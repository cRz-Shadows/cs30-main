﻿using System.Collections.Generic;

public class Card
{
    public int ID { get; }
    public float X { get; set; }
    public float Y { get; set; }

    public Card(int id, float x, float y)
    {
        this.ID = id;
        this.X = x;
        this.Y = y;
    }
}


public class Board
{
    private readonly int ID;
    private readonly List<Card> cards = new List<Card>();

    public Board(int id)
    {
        this.ID = id;
    }

    public int GetID()
    {
        return ID;
    }

    public void AddCard(Card card)
    {
        cards.Add(card);
    }

    public void DeleteCardByID(int cardID)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            if (cards[i].ID == cardID)
            {
                cards.RemoveAt(i);
                return;
            }
        }
    }

    public bool MoveCard(int cardID, float x, float y)
    {
        foreach(Card c in cards)
        {
            if (c.ID == cardID)
            {
                c.X = x;
                c.Y = y;
                return true;
            }
        }

        return false;
    }

    public List<Card> GetCards()
    {
        return cards;
    }
}
