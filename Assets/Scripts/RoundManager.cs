using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoundManager : MonoBehaviour
{
    public GameObject ActivityCard;
    public GameObject EventCard;
    public Sprite[] ActivitySprites;
    public Sprite[] EventSprites;
    public bool Spectator;
    public EnumGameRound CurrentRound { get; set; }
    public int SessionID { get; set; }
    public int OurBoardID { get; set; }
    public int CurrentBoardID { get; set; }
    private readonly ConcurrentQueue<Action> gameUpdates = new ConcurrentQueue<Action>();
    private readonly Dictionary<int, Dictionary<int, GameObject>> spawnedCards = new Dictionary<int, Dictionary<int, GameObject>>();

    void Start()
    {
        CurrentRound = EnumGameRound.MAIN_MENU;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void Update()
    {
        while (gameUpdates.TryDequeue(out Action action))
        {
            action();
        }
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        GameObject planDeck = GameObject.Find("BACK-act-plan-01");
        GameObject contextDeck = GameObject.Find("BACK-act-context-01");
        GameObject impDeck = GameObject.Find("BACK-act-imp-01");
        GameObject writeupDeck = GameObject.Find("BACK-act-write-up-01");
        GameObject canvas = GameObject.Find("Canvas");
        if (planDeck != null)
            planDeck.GetComponent<SpawnCard>().Active = false;
        if (contextDeck != null)
            contextDeck.GetComponent<SpawnCard>().Active = false;
        if (impDeck != null)
            impDeck.GetComponent<SpawnCard>().Active = false;
        if (writeupDeck != null)
            writeupDeck.GetComponent<SpawnCard>().Active = false;
        if (canvas != null)
        {
            if (CurrentRound.IsActivityRound() && CurrentRound != EnumGameRound.PLAN_ACTIVITY)
            {
                canvas.GetComponent<RoundTimer>().startingSeconds = WebConfigurationManager.ROUND_TIMER_MAX;
                canvas.GetComponent<RoundTimer>().ResetTimer();
                canvas.GetComponent<RoundTimer>().StartTimer();
            }
        }
        if (CurrentRound == EnumGameRound.WAITING_LOBBY)
        {
            GameObject sessionText = GameObject.Find("Canvas/SessionID");
            sessionText.GetComponent<Text>().text = "SessionID: " + SessionID;
        }
        if (CurrentRound.IsActivityRound() && CurrentBoardID == OurBoardID && !Spectator)
        {
            GameObject deck = GetDeckForGameround(CurrentRound);
            if (deck != null)
                deck.GetComponent<SpawnCard>().Active = true;
        }
        if (Spectator)
        {
            GameObject nextPlayer = GameObject.Find("EventSystem/Canvas2/NextPlayer");
            if (nextPlayer != null)
                nextPlayer.SetActive(true);
        }
    }

    public void EnqueueAction(Action action)
    {
        gameUpdates.Enqueue(action);
    }

    public void ChangeGameRound(EnumGameRound gameRound, int boardID)
    {
        //if (CurrentRound != gameRound)
        SceneLoader.Load(gameRound);

        if (boardID == -1)
            boardID = OurBoardID;

        SpawnEventCard.canDraw = OurBoardID == boardID;

        CurrentRound = gameRound;
        if (!Spectator)
            CurrentBoardID = boardID;
        UpdateVisibilities();
    }

    public void NextPlayer()
    {
        CurrentBoardID++;
        if (!spawnedCards.ContainsKey(CurrentBoardID))
            CurrentBoardID = 0;
        UpdateVisibilities();
    }

    public Sprite GetActivitySprite(string name)
    {
        return GetSprite(ActivitySprites, name);
    }

    public Sprite GetEventSprite(string name)
    {
        return GetSprite(EventSprites, name);
    }

    public GameObject SpawnCard(int boardID, int cardID, string name, float x, float y)
    {
        GameObject newActivityCard = SpawnSprite(ActivityCard, GetActivitySprite(name), x, y);
        RegisterCard(boardID, cardID, newActivityCard);
        newActivityCard.SetActive(boardID == CurrentBoardID);

        return newActivityCard;
    }

    public GameObject SpawnCard(Sprite sprite, float x, float y)
    {
        GameObject newActivityCard = SpawnSprite(ActivityCard, sprite, x, y);
        RegisterCard(OurBoardID, newActivityCard.GetInstanceID(), newActivityCard);

        return newActivityCard;
    }

    public GameObject SpawnEventRoundCard(string name, float x, float y)
    {

        getEventCardPile(name).GetComponent<SpawnEventCard>().removeEventCard(name);
        return SpawnSprite(EventCard, GetEventSprite(name), x, y, false);
    }

    public void MoveCard(int boardID, int cardID, float x, float y)
    {
        if (spawnedCards.ContainsKey(boardID) && spawnedCards[boardID].ContainsKey(cardID))
        {
            Vector3 newPos = new Vector3(x, y, -1);
            spawnedCards[boardID][cardID].transform.position = newPos;
        }
        else
        {
            Debug.Log("Couldn't find card");
        }
    }

    public void DeleteCard(int boardID, int cardID)
    {
        if (spawnedCards.ContainsKey(boardID) && spawnedCards[boardID].ContainsKey(cardID))
        {
            Destroy(spawnedCards[boardID][cardID]);
            spawnedCards[boardID].Remove(cardID);
        }
    }

    public void UpdateSprite(int boardID, int cardID, string name)
    {
        if (spawnedCards.ContainsKey(boardID) && spawnedCards[boardID].ContainsKey(cardID))
        {
            GameObject card = spawnedCards[boardID][cardID];
            card.GetComponent<SpriteRenderer>().sprite = GetActivitySprite(name);
        }
    }

    private GameObject SpawnSprite(GameObject gameObj, Sprite sprite, float x, float y, bool activity = true)
    {
        Vector3 pos = new Vector3(x, y, -1);
        GameObject newGameObject = GameObject.Instantiate(gameObj, pos, Quaternion.identity);
        SpriteRenderer cardSpriteRenderer = newGameObject.GetComponent<SpriteRenderer>();
        cardSpriteRenderer.sprite = sprite;
        if (activity)
        {
            SpriteDrag spriteDrag = newGameObject.GetComponent<SpriteDrag>();
            spriteDrag.ParentObjectID = newGameObject.GetInstanceID();
            DontDestroyOnLoad(newGameObject);
        }
        Debug.Log("Spawned sprite: " + newGameObject.GetInstanceID());

        return newGameObject;
    }

    private void RegisterCard(int boardID, int cardID, GameObject card)
    {
        if (spawnedCards.ContainsKey(boardID))
        {
            spawnedCards[boardID].Add(cardID, card);
        }
        else
        {
            Dictionary<int, GameObject> dict = new Dictionary<int, GameObject>()
            {
                { cardID, card }
            };

            spawnedCards.Add(boardID, dict);
        }
    }

    private void UpdateVisibilities()
    {
        foreach(var entry in spawnedCards)
        {
            foreach (var cardObj in entry.Value)
            {
                if (cardObj.Value != null)
                    cardObj.Value.SetActive(entry.Key == CurrentBoardID);
            }
        }
    }

    private GameObject GetDeckForGameround(EnumGameRound round)
    {
        return round switch
        {
            EnumGameRound.PLAN_ACTIVITY => GameObject.Find("BACK-act-plan-01"),
            EnumGameRound.CONTEXT_ACTIVITY => GameObject.Find("BACK-act-context-01"),
            EnumGameRound.IMP_ACTIVITY => GameObject.Find("BACK-act-imp-01"),
            EnumGameRound.WRITEUP_ACTIVITY => GameObject.Find("BACK-act-write-up-01"),
            _ => GameObject.Find("BACK-act-plan-01"),
        };
    }

    private Sprite GetSprite(Sprite[] sprites, string name)
    {
        Sprite sprite = sprites[0];
        for (int i = 0; i < sprites.Length; i++)
        {
            if (sprites[i].name == name)
            {
                sprite = sprites[i];
                break;
            }
        }

        return sprite;
    }

    private GameObject getEventCardPile(string name)
    {
        if(name.StartsWith("Event Deck - Red"))
        {
            return GameObject.Find("Event_Card_Draw_Red");
        }
        if(name.StartsWith("Event Deck - Green"))
        {
            return GameObject.Find("Event_Card_Draw_Green");
        }
        if(name.StartsWith("Event Deck - Blue"))
        {
            return GameObject.Find("Event_Card_Draw_Blue");
        }
        return GameObject.Find("Event_Card_Draw_Yellow");
    }
}
