<div id="top"></div>

<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/blob/main/CONTRIBUTING.md">Contributers</a>
<a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/issues">- Issues</a>
<a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/blob/main/LICENSE">- Licence</a>



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main">
    <img src="Assets/png/png/Logo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">How To Fail Your Research Degree</h3>

  <p align="center">
    How to Fail Your Research Degree is an educational game for 1 - 4 players or teams.

You are a student, undertaking a master's degree at an unusually busy and calamitous stage of your life. Can you plan and undertake your research well enough to pass whilst dealing with a flooded library, your sister's unexpected wedding, and the many other distractions of life? 
    <br />
    <a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://alistairhampton.itch.io/howtofailyourresearchdegree">View Demo</a>
    ·
    <a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/issues">Report Bug</a>
    ·
    <a href="https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#setup-and-build-unity">Setup and Build Unity</a></li>
        <li><a href="#setup-on-server">Setup On Server</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

How to Fail Your Research Degree is an existing tabletop game developed by Daisy Abbott at the Glasgow School of Art. Its primary purpose is to encourage light-hearted engagement with the various academic skills and activities necessary to undertake postgraduate research and the risks and pitfalls that can affect a research degree.  It is particularly suitable for taught master's and MRes students but can be equally useful for students in the first year of a PhD, or even final year undergraduates who are undertaking independent research projects. The game can also be used to (re)familiarise early career researchers to the process of managing a research project, and has been shown to be useful in introducing the terminology of research to novice researchers or those with English as an additional language.  Full information about the game including rules, resources, and evaluation is available at http://howtofailyourresearchdegree.com/

Digital educational games are not necessarily better than tabletop games. Each platform has advantages and disadvantages. That said, a digital version would be incredibly beneficial to increase the reach and impact of this game (which has been conclusively shown to be effective in achieving important learning outcomes for novice researchers).

* [You can find the full set of rules to the game here!](http://howtofailyourresearchdegree.com/game-rules.pdf)

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Unity](https://unity.com/)
* [GameCI](https://game.ci/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

* [You can find the full set of rules to the game here!](http://howtofailyourresearchdegree.com/)

### Prerequisites

* Install [Unity](https://unity3d.com/get-unity/download) - Project Created Using Version 2020.3.21f1
* Install [Git](https://git-scm.com/downloads)
* Install [Visual Studio](https://visualstudio.microsoft.com/)

### Setup and Build Unity

1. Clone the repo
   ```sh
   git clone https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main
   ```

2. Open the project `cs30-main` in unity

3. In order to build the server, you will have to go to `file/Build Settings` in unity, select WebGL from the platform menu, tick all scenes other than `Event View Round 5`, make sure development build is unticked and code optimisation is set to speed, then click `build and run`.

4. You should now have the most recent build of the game
<p align="right">(<a href="#top">back to top</a>)</p>

### Setup On Server

1. Clone the repo
   ```sh
   git clone https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main
   ```

2. Open the project `cs30-main` in unity - this will create the `websocket-sharp.dll` file you will need later.

3. You should now place the server executable file `UnityWebServer.exe` we provided into a directory with the dependencies: 
- `Newtonsoft.Json.dll` - Use visual studio's Nuget extension or any other means you are aware of to get this file
- `websocket-sharp.dll` - You can find this in cs30-main/Assets/Packages/WebSocketSharp-netstandard.1.0.1/netstandard2.0
- `config.json` - This should have been sent with the `UnityWebServer.exe` file. You can change the IP and the round timer by changing this file

4. You should now be able to run the executable using `UnityWebServer.exe` and a terminal should show up with the running server.
<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

The pandemic has been particularly lonely and challenging for postgraduates, who find themselves with less structure and support than other learning and research cohorts. Although online community building has been widely attempted across HE, as we move into COVID recovery many research students still lack peer networks. This game offers a tool to departments and researcher developers for fostering spaces of peer experience sharing. It can be used as a light-hearted route into rebuilding lost communities, and upskill new researchers in the project management and contingency planning skills that will smooth their research journey.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->
## Roadmap

Features out of the scope of our project:
- [Streamline facilitator hosting with settings] Feature 1
- [Cards Snapping Into Place] Feature 2
    - [Update mesh grid for sideways building] Nested Feature
- [Highlight the Active Deck] Feature 3
- [Allow collaberation within small team groups] Feature 4
- [Allow facilitator to highlight areas for discussion] Feature 5

See the [open issues](https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

This project is licensed under the GNU Affero General Public License v3.0. See `LICENSE` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Product Owner:
Alistair Hampton - 2459255h@student.gla.ac.uk

Other Roles:
Craig Livingstone - 2470470l@student.gla.ac.uk
Zoltan Sojtory   - 2442602s@student.gla.ac.uk
Zhaoyi Wang      - 2572556w@student.gla.ac.uk

Coach:
Jakub Graczyk    - 2400144g@student.gla.ac.uk

Project Link: [https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main](https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* [Unity](https://unity.com/)
* [GameCI](https://game.ci/)
* [Gitlab](https://about.gitlab.com/)
* [Figma](https://www.figma.com/)
* [LP Games](https://www.youtube.com/watch?v=M2BZr02uai0)
* [Itch.io](https://itch.io/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-url]: https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/graphs/contributors

[issues-url]: https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/issues/

[license-url]: https://stgit.dcs.gla.ac.uk/team-project-h/2021/cs30/cs30-main/-/blob/main/LICENSE
